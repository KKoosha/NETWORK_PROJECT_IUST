import java.io.*;
import java.net.Socket;

/**
 * Created by KK on 12/13/2015.
 *
 */
public class Main {

    public static final String SERVER_HOSTNAME = "localhost";
    public static final int SERVER_PORT = 2327;
    public static String name;
    public static String streamName;
    public static String fileName;
    public static boolean isOnReceive=false;

    public static void main(String[] args)
    {
        BufferedReader in = null;
        PrintWriter out = null;

        try
        {
            Socket socket = new Socket(SERVER_HOSTNAME, SERVER_PORT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            System.out.println("Connected to server " + SERVER_HOSTNAME + ":" + SERVER_PORT);
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            System.exit(-1);
        }


        Sender sender = new Sender(out);
        sender.start();


        try

        {
            String message;

            while ((message = in.readLine()) != null) {
                String[] messageContents = message.split("#");
                System.out.println(message);
                if(messageContents[0].equals("Reg")) {
                    name = message.substring(7);
                    System.out.println("my name is "+name);

                }
                if(message.equals("stream#ok"))
                    sender.fileReceiver();
                if(messageContents[0].equals("streamreq") & !messageContents[1].equals("nok")&!(messageContents.length==2)){
                    String udpReceiverIP = messageContents[2];
                    String udpReceiverPort = messageContents[3];
                    System.out.println(udpReceiverIP+"  "+udpReceiverPort);
                    //open udp
                    System.out.println("Now i have Next client udp port & ip wait for start ");
                    FileSender fileSender = new FileSender(udpReceiverPort);
                    fileSender.start();
                }

                if(messageContents[0].equals("streamreq")&&messageContents.length==2){

                    streamName = messageContents[1];

                }


            }

        } catch (Exception ioe)

        {
            System.err.println("Connection to server broken.");
            ioe.printStackTrace();

        }


    }


}


class Sender extends Thread

{
    public static String myUDPPort="8760";
    public static String myUDPIP = "127.0.0.1";
    private PrintWriter mOut;

    public Sender(PrintWriter aOut)

    {
        mOut = aOut;
    }

    public void run()

    {
        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            while (!isInterrupted()) {
                String message = in.readLine();
                String[] messageContents = message.split("#");

                if(messageContents[0].equals("streamreq")&&messageContents.length==2){

                        Main.fileName = messageContents[1];

                }

                if(message.equals("streamok"))
                    message = "stream#"+myUDPIP+"#"+myUDPPort;
                mOut.println(message);
                mOut.flush();
                if(message.equals("streamok")) {


                }
            }

        } catch (IOException ioe) {

        }

    }
    public void fileReceiver(){
        System.out.println("Now i can receive file");
        FileReceiver fileReceiver = new FileReceiver(myUDPPort);
        fileReceiver.start();
    }

}
