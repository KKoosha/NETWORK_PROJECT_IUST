import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by KK on 12/16/2015.
 */
public class MyFileWriter {
    byte data[];
    String port;
    public MyFileWriter(byte[] data,String port){
        this.data = data;
        this.port = port ;
        writingFile();

    }
    public void writingFile(){

        File file ;
        String directory=port+"Test";
        System.out.println("Writing file in "+directory);
        if(!new File("E:/"+directory).exists()) {
            boolean b = new File("E:/"+directory).mkdir();
            System.out.println(b);
        }
        try {
            file = new File("E:/"+directory+"/"+Main.streamName);
            FileOutputStream outputStream = new FileOutputStream(file);
            try {
                outputStream.write(data);
                outputStream.flush();
                outputStream.close();
                MyFileReader.directory = "E:/"+directory+"/"+Main.streamName;
                System.out.println("Finished writing file");
                Main.isOnReceive = false;
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
