import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Created by KK on 12/16/2015.
 */
public class FileReceiver extends Thread {

    String port;
    DatagramPacket packet;
    byte[] finalData = new byte[0];
    byte[] receiveData;
    ArrayList list = new ArrayList<>();
    DatagramSocket socket = null;
    public FileReceiver(String port){
        this.port = port ;
        try {
            socket = new DatagramSocket(Integer.parseInt(port));
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        do {


            Main.isOnReceive = true;
            byte[] buf = new byte[65507];
             packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);

                System.out.println("get file "+ packet.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
            receiveData= packet.getData();
            addToFinalData(receiveData);


           // System.out.println("Packet i get:");
            //System.out.println(packet.getData().toString()+"  "+new String(packet.getData()));

          //  System.out.println();

        }while (!new String(packet.getData()).contains("finished"));
        System.out.println("Finished Come");
        new MyFileWriter(finalData,port);
    }

    public void addToFinalData(byte[] data){
        byte[] temp;
        temp =finalData;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write( temp );
            outputStream.write( data );
        } catch (IOException e) {
            e.printStackTrace();
        }
        finalData= outputStream.toByteArray( );

    }
}
