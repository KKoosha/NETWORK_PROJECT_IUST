import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by KK on 12/16/2015.
 */
public class MyFileReader {

    static String  directory = "E:/"+Main.fileName;
    public  byte[] getFileBytes() throws IOException {
        File file = new File(directory);

        byte[] b = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(b);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("read file from"+directory);
        return b;
    }
}
