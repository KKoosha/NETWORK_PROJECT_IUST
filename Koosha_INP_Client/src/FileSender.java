import com.sun.deploy.util.ArrayUtil;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.*;
import java.util.Arrays;

/**
 * Created by KK on 12/16/2015.
 */
public class FileSender extends Thread{

    String port ;
    public FileSender(String port){
        this.port = port ;
    }




    @Override
    public void run() {
        System.out.println("Sending file is called " +Main.isOnReceive);
        while (Main.isOnReceive){
            System.out.print("");
        }

        System.out.println("not in loop");

        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        byte[] chunk = new byte[65507];
        byte[] buf = new byte[0];

        int startIndex=0;
        int endIndex=0;

        try {
            buf=new MyFileReader().getFileBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InetAddress address = null;
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        if(buf.length>65507)
            endIndex = 65507;
        else
            endIndex = buf.length;
        boolean i =true;
        while (i) {
            DatagramPacket packet;
            if(endIndex<buf.length){

                chunk = Arrays.copyOfRange(buf, startIndex, endIndex);
                packet = new DatagramPacket(chunk, chunk.length,
                        address, Integer.parseInt(port));


            }

            else{
                i=false;
                chunk = "finished".getBytes();
                System.out.println("now send finished "+new String (chunk));
                packet = new DatagramPacket(chunk, chunk.length,
                        address, Integer.parseInt(port));
                System.out.println(new String (packet.getData()));

            }

            try {
                System.out.println("packet sendContent is ");
              //  System.out.println(new String (packet.getData()));

                socket.send(packet);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startIndex=endIndex;
                int diff = buf.length - endIndex;
                System.out.println(diff + " bytes must send");
                if(diff>65507) {
                    endIndex += 65507;
                    System.out.println("65507 addedto end index");

                }
                else if (diff > 0 && diff <= 65507) {
                    System.out.println(diff+" added to End index");
                    endIndex += diff;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Send Finished");



    }
}

