import ClientHandler.Client;
import ClientHandler.ClientListener;
import ClientHandler.ClientSender;
import ServerHandler.ServerCommunicator;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Koosha Esmaielzadeh  on 12/12/2015.
 */
public class Main {

    private static final int SERVER_PORT = 2327;
    public static void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            ServerCommunicator serverCommunicator = new ServerCommunicator();
            serverCommunicator.start();

            while (true){
                System.out.println("----------------------------------------------------------------");
                Socket socket = serverSocket.accept();
                Client client = new Client();
                client.setSocket(socket);
                client.setClientName("UnRegistered Client");
                ClientListener clientListener = new ClientListener(client,serverCommunicator);
                ClientSender clientSender = new ClientSender(client,serverCommunicator);
                client.setListener(clientListener);
                client.setSender(clientSender);
                clientListener.start();
                clientSender.start();
                serverCommunicator.addClient(client);
                System.out.println("----------------------------------------------------------------");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
