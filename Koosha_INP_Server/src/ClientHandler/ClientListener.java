package ClientHandler;

import ServerHandler.ServerCommunicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by KK on 12/12/2015.
 */
public class ClientListener extends Thread {
    Client client;
    ServerCommunicator serverCommunicator;
    BufferedReader reader;
    boolean isEnd = false;

    public synchronized void setIsEnd(boolean isEnd) {
        this.isEnd = isEnd;
        notify();
    }

    public ClientListener(Client client, ServerCommunicator serverCommunicator) throws IOException {
        this.client = client ;
        this.serverCommunicator = serverCommunicator ;
        Socket socket = client.getSocket();
        // bastar baraye listen kardan
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        System.out.println("CLIENT LISTENER IS STARTED ");
        while (!isEnd&&!isInterrupted()){
            try {
                //read from client
                String message = reader.readLine();
                if (message.equals(null)){
                    continue;
                }
                System.out.println("Message from Client"+client.getClientName());
                serverCommunicator.clientMessagesHandler(message,client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Calling interupt For Sender in Listener");
        client.getSender().interrupt();
        serverCommunicator.deleteClient(client);
    }
}
