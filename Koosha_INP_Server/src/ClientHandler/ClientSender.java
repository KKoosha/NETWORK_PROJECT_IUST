package ClientHandler;

import ServerHandler.ServerCommunicator;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by KK on 12/12/2015.
 */
public class ClientSender extends Thread {

    Client client;
    boolean isEnd=false;
    ServerCommunicator serverCommunicator;
    PrintWriter writer;
    private ArrayList<String> clientSenderMessageQueue = new ArrayList<String>();
    public ClientSender(Client client, ServerCommunicator serverCommunicator) throws IOException {
        this.client = client;
        this.serverCommunicator = serverCommunicator ;
        Socket socket = client.getSocket();
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void sendMessageToClient(String message){
        writer.println(message);
        writer.flush();
    }


    public synchronized String getMessageFromQueue() throws InterruptedException {
        if(clientSenderMessageQueue.isEmpty()){
            wait();
        }
        String message = clientSenderMessageQueue.get(0);
        clientSenderMessageQueue.remove(0);
        return message;
    }
    public synchronized void addMessageToQueue(String message){
        clientSenderMessageQueue.add(message);
        notify();
    }

    public synchronized void setIsEnd(boolean isEnd) {
        this.isEnd = isEnd;
        notify();
    }

    @Override
    public void run() {
        System.out.println("CLIENT SENDER IS STARTED ");
        while (!isEnd&&!isInterrupted()){
            try {
                String message = getMessageFromQueue();
                sendMessageToClient(message);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        System.out.println("Calling interupt For Listener in Sender");
        client.getListener().interrupt();
        serverCommunicator.deleteClient(client);
    }


}
