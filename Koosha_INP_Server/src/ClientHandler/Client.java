package ClientHandler;

import java.net.Socket;

/**
 * Created by KK on 12/12/2015.
 */
public class Client {



    private String clientName;
    private Socket socket;
    private ClientListener listener;
    private ClientSender sender;
    private String UDP_IP;

    public String getUDP_PORT() {
        return UDP_PORT;
    }

    public void setUDP_PORT(String UDP_PORT) {
        this.UDP_PORT = UDP_PORT;
    }

    public String getUDP_IP() {
        return UDP_IP;
    }

    public void setUDP_IP(String UDP_IP) {
        this.UDP_IP = UDP_IP;
    }

    private String UDP_PORT;
    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public ClientListener getListener() {
        return listener;
    }

    public void setListener(ClientListener listener) {
        this.listener = listener;
    }

    public ClientSender getSender() {
        return sender;
    }

    public void setSender(ClientSender sender) {
        this.sender = sender;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
