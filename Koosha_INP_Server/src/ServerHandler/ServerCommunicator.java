package ServerHandler;

import ClientHandler.Client;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by KK on 12/12/2015.
 *  this class target :
 *      who should send ?
 *      what client should send ?
 *      where client should send?
 */

public class ServerCommunicator extends Thread {

    private ArrayList <Client> transferChain = new ArrayList<Client>();
    private ArrayList <ArrayList> messageQueue = new ArrayList<ArrayList>();

    private ArrayList <Client> onlineClients = new ArrayList<>();
    private Client senderClient;
    private Client chainSenderClient;
    private Client chainReceiverClient;

    public synchronized void addClient(Client client){
        onlineClients.add(client);
        System.out.println("********************************");
        System.out.println("Registered Clients till now: \n");
        for (int i=0;i< onlineClients.size();i++){
            System.out.println(onlineClients.get(i).getClientName());
        }
        System.out.println("********************************");
    }


    public synchronized void deleteClient(Client  client) {
        onlineClients.remove(client);

        if(transferChain.contains(client))
            transferChain.remove(client);
        try {
            client.getSender().setIsEnd(true);
            client.getListener().setIsEnd(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public synchronized void sendMessageToAll(String message){
        for (int i=0;i< onlineClients.size();i++){
            onlineClients.get(i).getSender().sendMessageToClient(message);
        }
    }
    public synchronized void addMessageToQueue(Client client,String message){
        ArrayList messageClientArrayList = new ArrayList();
        messageClientArrayList.add(message);
        messageClientArrayList.add(client);
        messageQueue.add(messageClientArrayList);
        notify();
    }
    public synchronized ArrayList getMessageFromQueue() throws InterruptedException {
        if(messageQueue.isEmpty())
            wait();
        ArrayList messageClientArrayList=  messageQueue.get(0);
        messageQueue.remove(0);
        return messageClientArrayList;
    }

    public synchronized void clientMessagesHandler(String message,Client client){
        senderClient = client;
        addMessageToQueue(client,message);
        if(message.contains("Reg")){
                System.out.println("some body want to register");
        }
    }

    @Override
    public void run() {

        System.out.println("SERVER COMMUNICATOR IS STARTED ");
        while (true){
            try {
                ArrayList messageClientArrayList = getMessageFromQueue();
                Client senderClient = (Client) messageClientArrayList.get(1);
                String message= (String) messageClientArrayList.get(0);
                String[] messageContent = message.split("#");
                System.out.println(messageContent.length+"  "+messageContent[0]);
                //Register Command is Come Now{
                    if (messageContent[0].equals("Reg")) {

                        if (!isNameExist(messageContent[1])) {
                            senderClient.setClientName(messageContent[1]);
                            senderClient.getSender().addMessageToQueue("Reg#Ok#" + senderClient.getClientName());
                        } else
                            senderClient.getSender().addMessageToQueue("Reg#NOk#Name Exist Try Another !!!!!!!!");
                    }


                //Bye is come now
                if(messageContent[0].equals("bye")){
                    if(senderClient.getClientName().equals("UnRegistered Client"))
                        senderClient.getSender().sendMessageToClient("Bye#nok#Register First");
                    else
                    deleteClient(senderClient);
                }
                //Stream Req is come Now
                if(messageContent[0].equals("streamreq")){
                    if(senderClient.getClientName().equals("UnRegistered Client"))
                        senderClient.getSender().sendMessageToClient("streamreq#nok#Register First");
                    else{
                    String streamName = message.substring(10);
                    sendMessageToAll(message);
                    transferChain.add(senderClient);
                }
                }

                //Stream
                if(messageContent[0].equals("stream")) {
                    if (senderClient.getClientName().equals("UnRegistered Client"))
                        senderClient.getSender().sendMessageToClient("Stream#nok#Register First");
                    else {
                        String udpIP;
                        String udpPort;

                        udpIP = messageContent[1];
                        udpPort = messageContent[2];


                        Client motherClient; //Sender of file
                        if (!transferChain.isEmpty()) {
                            motherClient = transferChain.get(0);
                            transferChain.remove(motherClient);
                            transferChain.add(senderClient);
                            senderClient.getSender().sendMessageToClient("stream#ok");
                            motherClient.getSender().sendMessageToClient("streamreq#ok#" + udpIP + "#" + udpPort);
                        } else
                            senderClient.getSender().sendMessageToClient("stream#nok#There is no Sender");
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public synchronized boolean isNameExist(String name){
        for(int i=0;i< onlineClients.size();i++){
            System.out.println(onlineClients.get(i).getClientName()+"=="+name+"  "+onlineClients.get(i).getClientName().equals(name));
            if(onlineClients.get(i).getClientName().equals(name))
                return true;
        }

        return false;
    }
}
